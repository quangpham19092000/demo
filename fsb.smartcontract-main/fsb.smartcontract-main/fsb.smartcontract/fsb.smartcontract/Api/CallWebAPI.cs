﻿using Common;
using fsb.smartcontract.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Model.Response;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace fsb.smartcontract.Api
{
    public class CallWebAPI
    {
        private static IConfiguration _configuration;
        private readonly IHttpContextAccessor _context;

        public static string _token { get; set; }
        public CallWebAPI(
            IConfiguration configuration,
            IHttpContextAccessor context)
        {
            _configuration = configuration;
            _context = context;
        }

        public T Get<T>(string url, Dictionary<string, string> dctParams = null, TimeSpan? overrideTimeout = null)
        {
            T obj = default(T);
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(_configuration["API:Url"]);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _context.HttpContext.Session.GetObject<string>(CommonConstants.TOKEN));
                    if (overrideTimeout != null)
                    {
                        client.Timeout = overrideTimeout.GetValueOrDefault(TimeSpan.FromSeconds(60));
                    }
                    BuildUrlParams(ref url, dctParams);
                    var response = client.GetAsync(url).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        string responseString = response.Content.ReadAsStringAsync().Result;
                        obj = JsonConvert.DeserializeObject<T>(responseString);
                    }
                    else
                    {
                        return GetError<T>(response);
                    }
                }
            }
            catch (Exception ex)
            {
                return GetError<T>(ex);
            }
            return obj;
        }
        public async Task<Response<FileDownloadInfo>> GetFileAsync(string url, Dictionary<string, string> dctParams = null, TimeSpan? overrideTimeout = null)
        {
            Response<FileDownloadInfo> res = new Response<FileDownloadInfo>();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(_configuration["API:Url"]);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _context.HttpContext.Session.GetObject<string>(CommonConstants.TOKEN));
                    if (overrideTimeout != null)
                    {
                        client.Timeout = overrideTimeout.GetValueOrDefault(TimeSpan.FromSeconds(60));
                    }
                    BuildUrlParams(ref url, dctParams);
                    var response = await client.GetAsync(url);
                    if (response.IsSuccessStatusCode)
                    {
                        var content = response.Content;
                        content.Headers.ContentType.CharSet = @"utf-8";
                        var contentStream = await content.ReadAsStreamAsync();
                        var fileName = content.GetFileName();
                        if (!string.IsNullOrEmpty(fileName))
                        {
                            var type = content.Headers.ContentType.ToString();
                            FileDownloadInfo file = new FileDownloadInfo();
                            file.FileName = fileName;
                            file.FileType = type;
                            file.Content = contentStream.GetByte();
                            res.Code = StatusCodes.Status200OK;
                            res.Data = file;
                        }
                        else
                        {
                            string responseString = await content.ReadAsStringAsync();
                            return JsonConvert.DeserializeObject<Response<FileDownloadInfo>>(responseString);
                        }
                    }
                    else
                    {
                        return GetError<Response<FileDownloadInfo>>(response);
                    }
                }
            }
            catch (Exception ex)
            {
                return GetError<Response<FileDownloadInfo>>(ex);
            }
            return res;
        }
        internal object Post<T>(string url)
        {
            throw new NotImplementedException();
        }

        public T Post<T>(string url, object JObj, TimeSpan? overrideTimeout = null)
        {
            T obj = default(T);
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(_configuration["API:Url"]);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _context.HttpContext.Session.GetObject<string>(CommonConstants.TOKEN));
                    //if (overrideTimeout != null)
                    // {
                    client.Timeout = overrideTimeout.GetValueOrDefault(TimeSpan.FromSeconds(60));
                    // }

                    var content = CreateHttpContent(JObj);
                    var res = client.PostAsync(string.Format(url), content).Result;
                    //response
                    if (res.IsSuccessStatusCode)
                    {
                        var responseString = res.Content.ReadAsStringAsync().Result;
                        obj = JsonConvert.DeserializeObject<T>(responseString);
                    }
                    else
                    {
                        return GetError<T>(res);
                    }
                }
            }
            catch (Exception ex)
            {
                return GetError<T>(ex);
            }
            return obj;
        }

        public async Task<T> PostFileAsync<T>(string url, string fileName, byte[] fileContent, object JObj, TimeSpan? overrideTimeout = null)
        {
            T obj = default(T);
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(_configuration["API:Url"]);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("multipart/form-data"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _token);
                    client.Timeout = overrideTimeout.GetValueOrDefault(TimeSpan.FromSeconds(600));

                    var formData = new MultipartFormDataContent();

                    var stringContent = CreateHttpContent(JObj);
                    stringContent.Headers.Add("Content-Disposition", "form-data; name=\"json\"");
                    formData.Add(stringContent, "json");

                    Stream stream = new MemoryStream(fileContent);
                    var streamContent = new StreamContent(stream);
                    streamContent.Headers.Add("Content-Type", "application/octet-stream");
                    streamContent.Headers.Add("Content-Disposition", "form-data; name=\"file\"; filename=\"" + "duongdan" + "\"");
                    formData.Add(streamContent, "file", "fileName");

                    var res = await client.PostAsync(string.Format(url), formData);

                    //response
                    if (res.IsSuccessStatusCode)
                    {
                        var responseString = res.Content.ReadAsStringAsync().Result;
                        obj = JsonConvert.DeserializeObject<T>(responseString);
                    }
                    else
                    {
                        return GetError<T>(res);
                    }
                }
            }
            catch (Exception ex)
            {
                return GetError<T>(ex);
            }
            return obj;
        }

        public async Task<T> PostFileAsync<T>(string url, IFormFile file, object JObj, TimeSpan? overrideTimeout = null)
        {
            T obj = default(T);
            try
            {
                MemoryStream memoryStream = new MemoryStream();
                await file.CopyToAsync(memoryStream);
                byte[] fileContent = memoryStream.ToArray();
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(_configuration["API:Url"]);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("multipart/form-data"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _token);
                    client.Timeout = overrideTimeout.GetValueOrDefault(TimeSpan.FromSeconds(600));

                    var formData = new MultipartFormDataContent();

                    var stringContent = CreateHttpContent(JObj);
                    stringContent.Headers.Add("Content-Disposition", "form-data; name=\"json\"");
                    formData.Add(stringContent, "json");

                    Stream stream = new MemoryStream(fileContent);
                    var streamContent = new StreamContent(stream);
                    streamContent.Headers.Add("Content-Type", file.ContentType);
                    streamContent.Headers.Add("Content-Disposition", file.ContentDisposition);
                    formData.Add(streamContent, file.Name, file.FileName);

                    var res = await client.PostAsync(string.Format(url), formData);

                    //response
                    if (res.IsSuccessStatusCode)
                    {
                        var responseString = res.Content.ReadAsStringAsync().Result;
                        obj = JsonConvert.DeserializeObject<T>(responseString);
                    }
                    else
                    {
                        return GetError<T>(res);
                    }
                }
            }
            catch (Exception ex)
            {
                return GetError<T>(ex);
            }
            return obj;
        }
        private T GetError<T>(Exception ex)
        {
            T obj = Activator.CreateInstance<T>();
            var props = typeof(T).GetRuntimeProperties();
            var msg = ex.InnerException.Message;
            if (msg.ToLower().Contains("canceled"))
            {

                foreach (var prop in props)
                {
                    if (prop.Name == "Code")
                        prop.SetValue(obj, StatusCodes.Status408RequestTimeout);
                    if (prop.Name == "Message")
                        prop.SetValue(obj, "Hết thời gian kết nối!");
                }
            }
            else
            {
                foreach (var prop in props)
                {
                    if (prop.Name == "Code")
                        prop.SetValue(obj, StatusCodes.Status500InternalServerError);
                    if (prop.Name == "Message")
                        prop.SetValue(obj, "Xảy ra lỗi khi kết nối đến máy chủ!");
                }
            }
            return obj;
        }
        private T GetError<T>(HttpResponseMessage response)
        {
            T obj = Activator.CreateInstance<T>();
            var props = typeof(T).GetRuntimeProperties();
            var code = response.StatusCode.ToString();
            foreach (var prop in props)
            {
                if (prop.Name == "Code")
                    prop.SetValue(obj, response.StatusCode);
                if (prop.Name == "Message")
                    prop.SetValue(obj, "Mã lỗi: " + code);
            }
            return obj;
        }
        private void BuildUrlParams(ref string url, Dictionary<string, string> dctParams)
        {
            int index = 0;
            if (dctParams != null && dctParams.Count > 0)
            {
                foreach (var item in dctParams)
                {
                    if (index == 0)
                    {
                        url += "?" + item.Key + "=" + item.Value;
                    }
                    else
                    {
                        url += "&" + item.Key + "=" + item.Value;
                    }
                    index += 1;
                }
            }
        }
        public HttpContent CreateHttpContent<T>(T obj)
        {
            if (obj == null)
                new StringContent("", Encoding.UTF8, "application/json");
            return new StringContent(JsonConvert.SerializeObject(obj), Encoding.UTF8, "application/json");
        }
    }
    public class FileDownloadInfo
    {
        public string FileName { get; set; }
        public string FileType { get; set; }
        public byte[] Content { get; set; }
    }
}
