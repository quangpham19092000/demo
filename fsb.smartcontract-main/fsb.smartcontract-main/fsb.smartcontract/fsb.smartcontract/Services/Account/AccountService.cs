﻿using fsb.smartcontract.Api;
using fsb.smartcontract.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Model.Response;
using Model.UserInfo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace fsb.smartcontract.Services.Account
{
    public class AccountService : Service<AccountService>, IAccountService
    {
        public AccountService(
            CallWebAPI api,
            ILogger<AccountService> logger,
            IHttpClientFactory factory,
            IHttpContextAccessor context
            ) : base(api, logger, factory, context)
        {
        }

        public Response<string> Login(LoginModel model)
        {
            Response<string> res = new Response<string>();
            try
            {
                string url = Path.AUTHENCATE_LOGIN;
                res = _api.Post<Response<string>>(url, model);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                res.Code = StatusCodes.Status500InternalServerError;
                res.Message = "Lỗi gọi api!";
            }
            return res;
        }
    }
}
