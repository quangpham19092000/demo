﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Entity
{
    public class Company : EntityBasecs
    {
        public string CompanyCode { get; set; }
        public string CompanyName { get; set; }
        public object DepartmentName { get; set; }
        public object DepartmentCode { get; set; }
    }
}
