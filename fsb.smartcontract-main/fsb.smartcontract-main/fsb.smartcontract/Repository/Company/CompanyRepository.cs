﻿using Entity.Context;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Model.Company;
using Model.Requests;
using Model.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Company
{
    public class CompanyRepository : Repository<CompanyRepository>, ICompanyRepository
    {
        private readonly IConfiguration _configuration;
        public CompanyRepository(
            ILogger<CompanyRepository> logger,
            IConfiguration configuration,
            FsbSmartContractContext context
            ) : base(context, logger)
        {
            _configuration = configuration;
        }
        public TableResponse<CompanyViewModel> GetListRole(SearchCompanyModel search)
        {
            TableResponse<CompanyViewModel> result = new TableResponse<CompanyViewModel>();
            result.Draw = search.Draw;

            try
            {
                var data = _context.Companies.Where(x => x.IsDeleted == false).Select(x => new CompanyViewModel
                {
                    Id = x.Id,
                    CompanyName = x.CompanyName,
                    CompanyCode = x.CompanyCode
                }).ToList();

                if (search.SearchValue != null)
                {
                    data = data.Where(x => x.CompanyName.ToLower().Contains(search.SearchValue.ToLower())).ToList();
                }

                var cnt = data.Count();
                result.Data = data.OrderBy(x => x.Id).Skip(search.Start).Take(search.Length).ToList();
                result.RecordsTotal = cnt;
                result.RecordsFiltered = cnt;
                result.Code = StatusCodes.Status200OK;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "");
                result.Code = StatusCodes.Status500InternalServerError;
                result.Message = "Xảy ra lỗi khi lấy danh sách công ty!";
            }
            return result;
        }
    }
}
